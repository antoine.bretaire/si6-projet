#include <iostream>
#include <libpq-fe.h>
using namespace std;

int main()
{
  const char server_encoding[] = "server_encoding";
  PGconn *connexion;
  connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=a.bretaire connect_timeout=10");

  if(PQping("host=postgresql.bts-malraux72.net port=5432 dbname=a.bretaire connect_timeout=10") == PQPING_OK);

  {
    cout << " * Utilisateur " << PQuser(connexion) << endl;
    cout << " * mot de passe :  " ;

    for(int i = 0 ; i < sizeof(PQpass(connexion)) ; i++)
    {
      cout << "*" ;
    }

    cout << endl;
    cout << " * base de données " << PQdb(connexion) << endl;
    cout << " * port TCP " << PQport(connexion) << endl;
    cout << " * chiffrement SSL " ;
    bool ssl = PQsslInUse(connexion);

    if(ssl == 1)
    {
      cout << "True" << endl;
    }
    else
    {
      cout << "False" << endl;
      cout << " * encodage " << PQparameterStatus(connexion, server_encoding) << endl;
      cout << " * version du protocole " << PQprotocolVersion(connexion) << endl;
      cout << " * version du serveur " << PQserverVersion(connexion) << endl;
      cout << " * version de la bibliothèque 'libpq' du client " << PQlibVersion() << endl;
    }

    PGresult *co;
    co = PQexec(connexion, "SELECT * FROM si6. \"Animal\" INNER JOIN si6. \"Race\" ON (\"Animal\".race_id = \"Race\".id) WHERE sexe = 'Femelle' AND si6. \"Race\".nom = 'Singapura';");

    if(PQresultStatus(co) == PGRES_TUPLES_OK || PQresultStatus(co) == PGRES_COMMAND_OK)
    {
      cout << "Commande ok" << endl;

      for(int a = 0; a < PQntuples(co); a++)
      {
        for(int b = 0; b < PQnfields(co); b++)
        {
          cout << " | " << PQgetvalue(co, a, b);
        }

        cout << endl;
      }

      cout << endl << " Exécution retourne " << PQntuples(co) << " enregistrements " << endl;
      PQclear(co);
    }
    else
    {
      cout << "Erreur" << endl;
    }
  }
  else
  {
    cout << " Accées au Serveur impossible (vérifier la connectivité)" << endl;
  }

  return 0;
}
